<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700%7CMaterial+Icons' rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />

    <!-- Theme style -->
    <!-- ===== Plugin CSS ===== -->
    <link href="{{route('index')}}/plugins/components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="{{route('index')}}/plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="{{route('index')}}/plugins/components/fullcalendar/fullcalendar.css" rel='stylesheet'>
    
    <link href="{{route('index')}}/css/cubic/animate.css" rel="stylesheet">

    <link href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/12.1.0/nouislider.css" />
    
    
    <link href="{{route('index')}}/css/cubic/style.css" rel="stylesheet">


    <link href="{{route('index')}}/css/style.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- AdminLTE App -->
    <!-- <script src="{{route('index')}}/js/AdminLTE/app.js"></script> -->
        
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
    <title>Autogig</title>
    <meta name="description" content="Autogig">
    <meta name="keywords" content="Autogig, Sales, Cars, Parts">

    <link rel="shortcut icon" type="image/png" href="{{route('index')}}/images/favicon.png"/>



  </head>

  <body>
  
    <div class="my-body" id="app">
      <app></app>
    </div>


    <script src="{{route('index')}}/public/js/app.js"></script>
    <script src="{{route('index')}}/js/myscript.js"></script> 

    <script src="{{route('index')}}/js/cubic/waves.js"></script>
    <!-- ===== Menu Plugin JavaScript ===== -->
    <script src="{{route('index')}}/js/cubic/sidebarmenu.js"></script>
    <!-- ===== Custom JavaScript ===== -->
    <!-- <script src="{{route('index')}}/js/cubic/custom.js"></script> -->

    <!-- ===== Style Switcher JS ===== -->
    <script src="{{route('index')}}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/12.1.0/nouislider.min.js"></script>


  </body>

</html>