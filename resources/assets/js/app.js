import Vue from 'vue'
import App from './views/App'
import router from './routes.js';

import store from './store/index';

import VModal from 'vue-js-modal'
Vue.use(VModal)

import axios from 'axios'
Vue.prototype.axios = axios;

import swal from 'sweetalert2'
Vue.prototype.Swal = swal;

Vue.prototype.toast = swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 5000
});

Vue.prototype.global_store = store;
Vue.prototype.global_loading = true;


import VueGoodTablePlugin from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css'
Vue.use(VueGoodTablePlugin);


import 'vue2-dropzone/dist/vue2Dropzone.css'


Vue.use(require('vue-script2'));

if (process.env.MIX_APP_ENV === 'production') {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}


const app = new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
});