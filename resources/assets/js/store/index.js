import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


let index_url = "";
if (window.location.hostname.includes("localhost")) {
  index_url = "/autogig";
}

export default new Vuex.Store({
  state: {
    isLoggedIn: !!localStorage.getItem('token'),
    index_url: index_url,
    user: {},
    user_role: "",
    global_loading: true,
  },
  mutations: {
    loginUser(state) {
      state.isLoggedIn = true
    },
    logoutUser(state) {
      state.isLoggedIn = false
    },
    user(state,getUser) {
      state.user = getUser
    },
    user_role(state,role) {
      state.user_role = role
    },
    global_loading(state,load_type) {
      state.global_loading = load_type
    }
  },
  getters: {
    index_url : state => state.index_url,
    user : state => state.user,
    user_role : state => state.user_role,
    isLoggedIn : state => state.isLoggedIn,
    global_loading : state => state.global_loading,
  }
})