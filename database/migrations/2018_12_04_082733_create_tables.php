<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->string('email', 100);
            $table->string('password', 100);
            $table->string('forgot_password', 100)->nullable();
            $table->string('role',20)->nullable();
            $table->string('phone_number',21)->nullable();
            $table->string('address', 150)->nullable();
            $table->string('state', 80)->nullable();
            $table->string('country', 80)->nullable();
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('title', 100);
            $table->text('description')->nullable();
            $table->string('price', 40);
            $table->text('media')->nullable();
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->timestamps();
        });

        Schema::create('vehicle_fit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        });

        Schema::create('models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->integer('brand_id')->unsigned();
            $table->timestamps();
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        });

        Schema::create('selected_year', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 20);
            $table->integer('model_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->timestamps();
            $table->foreign('model_id')->references('id')->on('models')->onDelete('cascade');
        });

        Schema::create('trims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->integer('selected_year_id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->timestamps();
            $table->foreign('selected_year_id')->references('id')->on('selected_year')->onDelete('cascade');
        });

        Schema::create('products_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->integer('product_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('review');
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('products_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->text('media')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('quantity');
            $table->string('track',80);
            $table->text('order_code');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::create('custom_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('vin',80);
            $table->string('brand',80);
            $table->string('model',80);
            $table->year('year_selected');
            $table->string('trim',80);
            $table->string('part_number',80)->nullable();
            $table->string('part_name',80)->nullable();
            $table->integer('quantity');
            $table->text('comment')->nullable();
            $table->text('media');
            $table->string('track',80);
            $table->string('price',80);
            $table->text('order_code');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->text('activity');
            $table->text('user');
            $table->timestamps();
        });

    }


  


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
        Schema::drop('trims');
        Schema::drop('selected_year');
        Schema::drop('models');
        Schema::drop('vehicle_fit');
        Schema::drop('brands');
        Schema::drop('products_reviews');
        Schema::drop('products_media');
        Schema::drop('orders');
        Schema::drop('products');
        Schema::drop('categories');
        Schema::drop('custom_orders');
        Schema::drop('users');
    }
}
