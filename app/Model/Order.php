<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function default_image()
    {
        return $this->hasOne('App\Model\ProductMedia', 'product_id', 'product_id');
    }

}
