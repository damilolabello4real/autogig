<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function category()
    {
        return $this->belongsTo('App\Model\Category', 'category_id', 'id');
    }

    public function default_image()
    {
        return $this->hasOne('App\Model\ProductMedia', 'product_id', 'id');
    }

    public function images()
    {
        return $this->hasMany('App\Model\ProductMedia', 'product_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Model\ProductReview', 'product_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany('App\Model\Order', 'product_id', 'id');
    }

    public function singleorder()
    {
        return $this->hasOne('App\Model\Order', 'product_id', 'id');
    }

}
