<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AllOrder extends Model
{
    protected $table = 'all_orders';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
