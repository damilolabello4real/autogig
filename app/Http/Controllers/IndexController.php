<?php

namespace App\Http\Controllers;

use App\Model\User;

use Barryvdh\Debugbar;

use Faker\Factory;

use SnappyPDF;

use DateTime;

use File;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Url;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request\Validator;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_encode;


use Illuminate\Support\Facades\DB;

class IndexController extends ExtendedFunctions
{

    public function index($any = null){ 

      return view('index');

    }
}