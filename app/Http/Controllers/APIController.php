<?php

namespace App\Http\Controllers;

use App\User;
use App\Model\Brand;
use App\Model\BrandModel;
use App\Model\Year;
use App\Model\Trim;
use App\Model\Product;
use App\Model\ProductMedia;
use App\Model\ProductBrandLogo;
use App\Model\NewsletterSubscription;
use App\Model\ProductReview;
use App\Model\ProductRecentView;
use App\Model\ContentPage;
use App\Model\AllOrder;
use App\Model\CustomOrder;
use App\Model\Order;
use App\Model\Category;
use App\Model\Message;
use App\Model\QlEditor;
use App\Model\VehicleFit;
use App\Model\Setting;

use App\Rules\FeaturedproductCount;
use App\Rules\HotproductCount;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Support\Facades\Mail;

class APIController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = User::where("email",$request->get('email'))->first();

        return response()->json(compact('user','token'));
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:3|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:3',
            'phone_number' => 'required',
            'address' => 'required',
            'state' => 'required',
        ]);


        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'phone_number' => $request->get('phone_number'),
            'address' => $request->get('address'),
            'state' => $request->get('state'),
            'role' => 'user',
        ]);

        $user_email = $request->get('email');

        Mail::send("emails.new_user", [
            'user_name' => $request->get('name'),
            ], function($message) use ($user_email){
                        $message->subject('Welcome to Autogig International');
                        $message->to($user_email);
        });

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

    public function changePassword(Request $request)
    {
        \Debugbar::error($request->get("current_password"));

        try {
            if ($user = JWTAuth::parseToken()->authenticate()) {
        
                if(Hash::check($request->get("current_password"), $user->password)){
                    $user->password = Hash::make($request->get('password'));
                    $user->save();
        
                    $msg = "success";
                }else{
                    $msg = "error";
                }

                if($request->get("password") != $request->get("password_confirmation")){
                    $msg = "password_confirmation_failed";
                }
        
            }

        } catch (JWTException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } 


        return response()->json(compact('msg'),201);
    }

    public function getAuthenticatedUser(){
                    
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function logout(Request $request) 
    {
        // Get JWT Token from the request header key "Authorization"
        $token = $request->header('Authorization');
        // Invalidate the token
        try {
            JWTAuth::invalidate($token);
            return response()->json([
                'status' => 'success', 
                'message'=> "User successfully logged out."
            ]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
              'status' => 'error', 
              'message' => 'Failed to logout, please try again.'
            ]);
        }
    }

    public function dashboarddetails(){
        $users_count = User::all()->count();
        $product_count = Product::all()->count();
        $brand_count = brand::all()->count();
        $model_count = BrandModel::all()->count();
        $review_count = ProductReview::all()->count();
        $all_orders_count = AllOrder::where("tracking_status","!=","cancelled")->get()->count();
        $pending_orders_count = AllOrder::where("tracking_status","processing")->orWhere('tracking_status', 'shipping')->orWhere('tracking_status', 'new')->get()->count();
        $delivered_orders_count = AllOrder::where("tracking_status","delivered")->get()->count();
        $normal_orders_count = AllOrder::where("order_type","normal")->where("tracking_status","!=","cancelled")->get()->count();
        $custom_orders_count = AllOrder::where("order_type","custom")->where("tracking_status","!=","cancelled")->get()->count();

        $current_year_month = date('Y-m');
        

        $chartData[] = ["Year","Delivered","Uncompleted"];

        for ($i=11; $i > -1; $i--) { 
            $submonth = "-". $i . " months";
            $last_x_month = date("Y-m", strtotime($submonth));
            $short_last_x_month = date("m/y", strtotime($submonth));
            $delivered = AllOrder::where("created_at","Like",$last_x_month."%")->where("tracking_status","delivered")->get()->count();
            $uncompleted = AllOrder::where("created_at","Like",$last_x_month."%")->where("tracking_status","!=","delivered")->where("tracking_status","!=","cancelled")->get()->count();
            $get = [];
            $get[0] = $short_last_x_month;
            $get[1] = $delivered;
            $get[2] = $uncompleted;
            $chartData[] = $get;
        }

        $user = JWTAuth::parseToken()->authenticate();

        return response()->json(
            compact(
                'user',
                'product_count',
                'users_count',
                'brand_count',
                'model_count',
                'review_count',
                'all_orders_count',
                'pending_orders_count',
                'delivered_orders_count',
                'normal_orders_count',
                'custom_orders_count',
                'chartData'
            ));
    }

    public function navcount(){

        $newOrderCount = AllOrder::where("tracking_status","new")->count();

        return response()->json(compact('newOrderCount'));

    }

    public function getallorders($order_type = null){

        $all_orders = AllOrder::with('user')->where("tracking_status","!=","cancelled")->get();
        $new = AllOrder::where("tracking_status","new")->get()->count();
        $processing = AllOrder::where("tracking_status","processing")->get()->count();
        $shipping = AllOrder::where("tracking_status","shipping")->get()->count();
        $delivered = AllOrder::where("tracking_status","delivered")->get()->count();
        $notdelivered = AllOrder::where("tracking_status","not delivered")->get()->count();


        if($order_type == "new"){
            $all_orders = AllOrder::with('user')->where("tracking_status","new")->get();
        }
        if($order_type == "processing"){
            $all_orders = AllOrder::with('user')->where("tracking_status","processing")->get();
        }
        if($order_type == "shipping"){
            $all_orders = AllOrder::with('user')->where("tracking_status","shipping")->get();
        }
        if($order_type == "delivered"){
            $all_orders = AllOrder::with('user')->where("tracking_status","delivered")->get();
        }
        if($order_type == "not delivered"){
            $all_orders = AllOrder::with('user')->where("tracking_status","not delivered")->get();
        }

        return response()->json(compact(
            'all_orders',
            'new',
            'processing',
            'shipping',
            'delivered',
            'notdelivered'
        ));

    }

    public function getorders(Request $request){

        try {
            if ($user = JWTAuth::parseToken()->authenticate()) {
                $user = User::where("email",$user->email)->first();

                if($request->get("order_code") == null){
                    $orders = AllOrder::with('user')->where("tracking_status","!=","cancelled")->where("user_id",$user->id)->get();
                }else{
                    $orders = AllOrder::with('user')->where("tracking_status","!=","cancelled")->where("user_id",$user->id)->where("order_code",$request->get("order_code"))->get();
                }

                return response()->json(compact('orders'));
                
            }
        } catch (\Throwable $th) {
            \Debugbar::error($th);
        }

    }

    public function getuserorder($user_id = null, $order_process = null){

        $all_orders = AllOrder::with('user')->where("tracking_status","!=","cancelled")->where("user_id",$user_id)->get();
        $new = AllOrder::where("tracking_status","new")->where("user_id",$user_id)->get()->count();
        $processing = AllOrder::where("tracking_status","processing")->where("user_id",$user_id)->get()->count();
        $shipping = AllOrder::where("tracking_status","shipping")->where("user_id",$user_id)->get()->count();
        $delivered = AllOrder::where("tracking_status","delivered")->where("user_id",$user_id)->get()->count();
        $notdelivered = AllOrder::where("tracking_status","not delivered")->where("user_id",$user_id)->get()->count();


        if($order_process == "new"){
            $all_orders = AllOrder::with('user')->where("user_id",$user_id)->where("tracking_status","new")->get();
        }
        if($order_process == "processing"){
            $all_orders = AllOrder::with('user')->where("user_id",$user_id)->where("tracking_status","processing")->get();
        }
        if($order_process == "shipping"){
            $all_orders = AllOrder::with('user')->where("user_id",$user_id)->where("tracking_status","shipping")->get();
        }
        if($order_process == "delivered"){
            $all_orders = AllOrder::with('user')->where("user_id",$user_id)->where("tracking_status","delivered")->get();
        }
        if($order_process == "not delivered"){
            $all_orders = AllOrder::with('user')->where("user_id",$user_id)->where("tracking_status","not delivered")->get();
        }

        $user = User::where("id",$user_id)->first();

        return response()->json(compact(
            'all_orders',
            'new',
            'processing',
            'shipping',
            'delivered',
            'notdelivered',
            'user'
        ));

    }

    public function getproductorder($product_id = null, $order_process = null, $order_type = null){

        $all_orders = AllOrder::with('user')->where("tracking_status","!=","cancelled")->where("product_id",$product_id)->where("order_type",$order_type)->get();
        $new = AllOrder::where("tracking_status","new")->where("product_id",$product_id)->where("order_type",$order_type)->get()->count();
        $processing = AllOrder::where("tracking_status","processing")->where("product_id",$product_id)->where("order_type",$order_type)->get()->count();
        $shipping = AllOrder::where("tracking_status","shipping")->where("product_id",$product_id)->where("order_type",$order_type)->get()->count();
        $delivered = AllOrder::where("tracking_status","delivered")->where("product_id",$product_id)->where("order_type",$order_type)->get()->count();
        $notdelivered = AllOrder::where("tracking_status","not delivered")->where("product_id",$product_id)->where("order_type",$order_type)->get()->count();


        if($order_process == "new"){
            $all_orders = AllOrder::with('user')->where("product_id",$product_id)->where("order_type",$order_type)->where("tracking_status","new")->get();
        }
        if($order_process == "processing"){
            $all_orders = AllOrder::with('user')->where("product_id",$product_id)->where("order_type",$order_type)->where("tracking_status","processing")->get();
        }
        if($order_process == "shipping"){
            $all_orders = AllOrder::with('user')->where("product_id",$product_id)->where("order_type",$order_type)->where("tracking_status","shipping")->get();
        }
        if($order_process == "delivered"){
            $all_orders = AllOrder::with('user')->where("product_id",$product_id)->where("order_type",$order_type)->where("tracking_status","delivered")->get();
        }
        if($order_process == "not delivered"){
            $all_orders = AllOrder::with('user')->where("product_id",$product_id)->where("order_type",$order_type)->where("tracking_status","not delivered")->get();
        }



        if($order_type == "custom"){
            $product = CustomOrder::where("id",$product_id)->first()->part_name;
        }else{
            $product = Order::where("product_id",$product_id)->first()->product_title;
        }

        return response()->json(compact(
            'all_orders',
            'new',
            'processing',
            'shipping',
            'delivered',
            'notdelivered',
            'product'
        ));

    }

    public function getorderid($id = null){

        $order = AllOrder::where("id",$id)->first();

        if($order->vin != null){
            $orderdetails = CustomOrder::with('user')->where("vin",$order->vin)->where("user_id",$order->user_id)->first();
        }else{
            $orderdetails = Order::with('user')->where("product_id",$order->product_id)->where("user_id",$order->user_id)->first();
        }

        return response()->json(compact('order','orderdetails'));

    }

    public function editorderid($id = null){

        $order = CustomOrder::with('user')->where("id",$id)->first();

        return response()->json(compact('order'));

    }

    public function updateOrderStatus($id = null, $status = null){
        $updateorder = AllOrder::where("id",$id)->first();
        $updateorder->tracking_status = $status;
        $updateorder->save();

        $success = "Order status updated successfully";

        $newOrderCount = AllOrder::where("tracking_status","new")->count();

        return response()->json(compact('success','newOrderCount'));
    }

    public function updateOrder(Request $request){

        $order = CustomOrder::where("id",$request->get("order_id"))->first();

        $user = User::where("id",$order->user_id)->first();

        $order->vin = $request->get("vin");
        $order->part_name = $request->get("part_name");
        $order->part_number = $request->get("part_number");
        $order->quantity = $request->get("quantity");
        $order->shipping = $request->get("shipping");
        $order->price = $request->get("price");
        $order->total = ($request->get("price") * $request->get("quantity")) + $request->get("shipping");

        $file =  $request->file("file");

        if($file != null){

            if($order->media != null || $order->media != ""){
                $path = base_path().'/'.$order->media; 
                unlink($path);
            }

            
            $file_title = $file->getClientOriginalName();
                    
            $file_ext = strtolower($file->getClientOriginalExtension());
            $rand_code  = "0";
            if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){
                
                for($j=0; $j<9; $j++) {
                    $min = ($j == 0) ? 1:0;
                    $rand_code .= mt_rand($min,9);
                }

                $file->move('images/productimages/', $rand_code.'.'.$file_ext);
                
                $order->media = $new_media = 'images/productimages/'.$rand_code.'.'.$file_ext;
                        
            }

        }

        $order->save();

        $official_email = Setting::where("skey","email")->first()->svalue;

        Mail::send("emails.orderupdated", [
            'name' => $user->name,
            'email' => $user->email,
            'brand' => $order->brand,
            'model' => $order->model,
            'year' => $order->year_selected,
            'trim' => $order->trim,
            'part_name' => $request->get("part_name"),
            'part_number' => $request->get("part_number"),
            'quantity' => $request->get("quantity"),
            'shipping' => $request->get("shipping"),
            'price' => $request->get("price"),
            'total' => ($request->get("price") * $request->get("quantity")) + $request->get("shipping"),
            'date' => date('d/m/Y H:i:s')
            ], function($message) use ($user,$request,$official_email){
                        $message->subject($request->get("part_name").' Order Updated - Autogig');
                        $message->to($user->email,$user->name);
                        $message->replyTo($official_email);
        });

        $success = "Order updated successfully";

        return response()->json(compact('success','new_media'));            
      

    }

    public function getcustomorders(){

        $orders = CustomOrder::with('user')->where("cart","yes")->get();

        return response()->json(compact('orders'));
    
    }

    public function getusers(){
        $users = User::with('orders')->with('custom_orders')->with('reviews')->get();
        $users_count = User::where("role","user")->count();
        $admins_count = User::where("role","!=","user")->count();

        $user = JWTAuth::parseToken()->authenticate();

        return response()->json(compact('user','users','users_count','admins_count'));
    }

    public function getuser($id = null){
        $user = User::where("id",$id)->first();

        return response()->json(compact('user'));
    }

    public function getcurrentuser(){
        try {
            
            if ($user = JWTAuth::parseToken()->authenticate()) {
                $user = User::where("email",$user->email)->first();

                return response()->json(compact('user'));
                
            }
        
        } catch (\Throwable $th) {
                //throw $th;
        }
    }

    public function updateUser(Request $request){

        $user = User::where("id",$request->get('id'))->first();

        $user->name = $request->get('name');
        $user->role = $request->get('role');
        $user->phone_number = $request->get('phone_number');
        $user->address = $request->get('address');
        $user->state = $request->get('state');

        $user->save();

        $success = "User updated successfully";

        return response()->json(compact('success'));

    }

    public function updatecurrentuser(Request $request){

        $request->validate([
            'name' => 'required|string|max:255',
            'phone_number' => 'required',
            'address' => 'required',
            'state' => 'required',
        ]);

        $user = User::where("id",$request->get('id'))->first();

        $user->name = $request->get('name');
        $user->phone_number = $request->get('phone_number');
        $user->address = $request->get('address');
        $user->state = $request->get('state');

        $user->save();

        $success = "User updated successfully";

        return response()->json(compact('success'));

    }

    public function deleteUser($id = null){
        User::where("id",$id)->delete();

        $success = "User deleted successfully";

        return response()->json(compact('success'));
    }


    public function addBrand($title=null){

        if(strpos($title,',') !== false){
            $arrayTitle = explode(',', $title);

            foreach ($arrayTitle as $value) {
                $brand = new Brand;
                $brand->title = $value;
                $brand->slug = str_slug($value, '-');
                $brand->save();
            }
        }else{
            $brand = new Brand;
            $brand->title = $title;
            $brand->slug = str_slug($title, '-');
            $brand->save();

            $success = "Brand(s) added successfully";
        }

        return response()->json(compact('success'));
    }


    public function addProductCategory($title=null){

        if(strpos($title,',') !== false){
            $arrayTitle = explode(',', $title);

            foreach ($arrayTitle as $value) {
                $category = new Category;
                $category->title = $value;
                $category->slug = str_slug($value, '-');
                $category->save();
            }
        }else{
            $category = new Category;
            $category->title = $title;
            $category->slug = str_slug($title, '-');
            $category->save();

            $success = "Category added successfully";
        }

        return response()->json(compact('success'));
    }
    
    public function deleteProductCategory($id=null){

        Category::where("id",$id)->delete();

        $success = "Category deleted successfully";

        return response()->json(compact('success'));
    }

    public function editProductCategory($id=null, $new_category=null){

        $category = Category::where("id",$id)->first();
        $category->title = $new_category;
        $category->slug = str_slug($new_category, '-');
        $category->save();

        $success = "Category updated successfully";

        return response()->json(compact('success'));
    }

    public function verifynewsletteremail($email=null){

        
        $rand_code = "";
        for($j=0; $j<30; $j++) {
            $min = ($j == 0) ? 1:0;
            $rand_code .= mt_rand($min,9);
        }

        $nls = NewsletterSubscription::where("email",$email)->first();

        if($nls == null){
            $nls = new NewsletterSubscription;
        
            $nls->email = $email;
            $nls->verification = "false";
            $nls->code = $rand_code;
            $nls->save();

            $verify_newsletter_email = route('index').'/verify-newsletter-email'.'/'.$rand_code;

            $official_email = Setting::where("skey","email")->first()->svalue;

            Mail::send("emails.verifynewsletteremail", [
                'email' => $email,
                'verify_newsletter_email' => $verify_newsletter_email,
                'date' => date('d/m/Y H:i:s')
                ], function($message) use ($email,$official_email){
                            $message->subject('Newsletter email verification from http://autogig.com.ng');
                            $message->to($email);
            });

            $message = "success";
            return response()->json(compact('message'));
        }else{
            $message = "exist";
            return response()->json(compact('message'));
        }

    }

    public function verifynewsletteremaildone($code = null){

        $nls = NewsletterSubscription::where("code",$code)->first();

        if($nls == null){
            $message = "error";
            return response()->json(compact('message'));
        }

        $nls->verification = "true";
        $nls->code = null;
        $nls->save();

        $message = "success";
        return response()->json(compact('message'));

    }

    public function getnewsletteremails(){
        $emails = NewsletterSubscription::where("verification","true")->get();

        return response()->json(compact('emails'));
    }


    public function addnewsletteremails($emails=null){

        if(strpos($emails,',') !== false){
            $arrayTitle = explode(',', $emails);

            foreach ($arrayTitle as $value) {
                $nls = new NewsletterSubscription;
                $nls->email = $value;
                $nls->verification = "true";
                $nls->save();
            }
        }else{
            $nls = new NewsletterSubscription;
            $nls->email = $emails;
            $nls->verification = "true";
            $nls->save();

            $success = "Email(s) added successfully";
        }

        return response()->json(compact('success'));
    }

    public function editnewsletteremail($id=null, $email=null){

        $nls = NewsletterSubscription::where("id",$id)->first();
        $nls->email = $email;
        $nls->save();

        $success = "Email updated successfully";

        return response()->json(compact('success'));
    }


    public function deletenewsletteremail($id=null){

        NewsletterSubscription::where("id",$id)->delete();

        $success = "Email deleted successfully";

        return response()->json(compact('success'));
    }


    public function deleteBrand($id=null){

        Brand::where("id",$id)->delete();

        $success = "Brand deleted successfully";

        return response()->json(compact('success'));
    }

    public function editBrand($id=null, $new_brand_name=null){

        $brand = Brand::where("id",$id)->first();
        $brand->title = $new_brand_name;
        $brand->slug = str_slug($new_brand_name, '-');
        $brand->save();

        $success = "Brand updated successfully";

        return response()->json(compact('success'));
    }


    public function getbrands(){
        $brands = Brand::all();

        return response()->json(compact('brands'));
    }

    public function getmodels($brand){
        $brand = Brand::where("slug",$brand)->first();

        $models = BrandModel::where("brand_id",$brand->id)->get();

        return response()->json(compact('models','brand'));
    }

    public function addModel($title=null, $brand_id=null){

        if(strpos($title,',') !== false){
            $arrayTitle = explode(',', $title);

            foreach ($arrayTitle as $value) {
                $model = new BrandModel;
                $model->title = $value;
                $model->brand_id = $brand_id;
                $model->slug = str_slug($value, '-');
                $model->save();
            }
        }else{
            $model = new BrandModel;
            $model->title = $title;
            $model->brand_id = $brand_id;
            $model->slug = str_slug($title, '-');
            $model->save();

            $success = "Model(s) added successfully";
        }

        return response()->json(compact('success'));
    }

    public function deleteModel($id=null){

        BrandModel::where("id",$id)->delete();

        $success = "Model deleted successfully";

        return response()->json(compact('success'));
    }

    public function editModel($id=null, $new_model_name=null){

        $model = BrandModel::where("id",$id)->first();
        $model->title = $new_model_name;
        $model->slug = str_slug($new_model_name, '-');
        $model->save();

        $success = "Model updated successfully";

        return response()->json(compact('success'));
    }

    


    public function getyears($brand, $model){

        $model = BrandModel::where("slug",$model)->first();

        $years = Year::where("model_id",$model->id)->get();

        return response()->json(compact('years','model'));
    }

    public function addYear($title=null, $model_id=null){


        $model = BrandModel::where("id",$model_id)->first();

        if(strpos($title,',') !== false){
            $arrayTitle = explode(',', $title);

            foreach ($arrayTitle as $value) {

                $year = new Year;
                $year->title = $value;
                $year->model_id = $model_id;
                $year->brand_id = $model->brand_id;
                $year->save();
            }
        }else{

            $year = new Year;
            $year->title = $title;
            $year->model_id = $model_id;
            $year->brand_id = $model->brand_id;
            $year->save();

            $success = "Year(s) added successfully";
        }

        return response()->json(compact('success'));
    }

    public function deleteYear($id=null){

        Year::where("id",$id)->delete();

        $success = "Year deleted successfully";

        return response()->json(compact('success'));
    }

    public function editYear($id=null, $new_year=null){

        $year = Year::where("id",$id)->first();
        $year->title = $new_year;
        $year->save();

        $success = "Year updated successfully";

        return response()->json(compact('success'));
    }





    public function gettrims($brand_id, $model_slug, $year){

        $model = BrandModel::where("slug",$model_slug)->first();

        $year = Year::where("title",$year)->where("model_id",$model->id)->first();

        $trims = Trim::where("selected_year_id",$year->id)->get();

        return response()->json(compact('trims','year'));
    }

    public function addTrim($title=null, $year_id=null){

        $year = Year::where("id",$year_id)->first();

        if(strpos($title,',') !== false){
            $arrayTitle = explode(',', $title);

            foreach ($arrayTitle as $value) {

                $trim = new Trim;
                $trim->title = $value;
                $trim->slug = str_slug($title, '-');
                $trim->selected_year_id = $year_id;
                $trim->model_id = $year->model_id;
                $trim->brand_id = $year->brand_id;
                $trim->save();
            }
        }else{

            $trim = new Trim;
            $trim->title = $title;
            $trim->slug = str_slug($title, '-');
            $trim->selected_year_id = $year_id;
            $trim->model_id = $year->model_id;
            $trim->brand_id = $year->brand_id;
            $trim->save();

            $success = "Trim(s) added successfully";
        }

        return response()->json(compact('success'));
    }

    public function deleteTrim($id=null){

        Trim::where("id",$id)->delete();

        $success = "Trim deleted successfully";

        return response()->json(compact('success'));
    }

    public function editTrim($id=null, $new_trim=null){

        $trim = Trim::where("id",$id)->first();
        $trim->title = $new_trim;
        $trim->save();

        $success = "Trim updated successfully";

        return response()->json(compact('success'));
    }

    public function getproductcategories(){

        $categories = Category::all();

        return response()->json(compact('categories'));
    }

    public function get_categories_brands_price(){
        $categories = Category::all();
        
        $brands = Brand::all();

        $minprice = Product::orderBy("price","asc")->first();
        $maxprice = Product::orderBy("price","desc")->first();

        return response()->json(compact('categories','brands','minprice','maxprice'));
    }

    public function categoriesForProducts(){

        $user = JWTAuth::parseToken()->authenticate();

        $categories = Category::all();

        $products = ProductMedia::where("product_id",null)->where("user_id",$user->id)->get();

        foreach ($products as $product) {
            $path = base_path().'/'.$product->media; 
            unlink($path);
            $product->delete();
        }

        return response()->json(compact('categories'));

    }

    public function getrandomcategories(){

        $categories = Category::inRandomOrder()->take(3)->get();

        return response()->json(compact('categories'));

    }

    public function products(){

        $products = Product::with('category')->with('default_image')->with('orders')->with('singleorder')->with('reviews')->get();        

        return response()->json(compact('products'));

    }

    public function productsfiltered(Request $request){

        $skip = $request->get('skip'); $take = $request->get('take');

        if($request->get('page') != null){
            if($request->get('skip') != $request->get('take')){
                $skip = ($request->get('page') - 1) * $take;
            }else{
                $skip = 0;
            }   
        }

        if($request->get('brand') != null){

            $brands = $request->get('brand');
            $vehicle_fit = [];
            $brand_ids = [];

            foreach ($brands as $brand) {
                $brand_id = Brand::where("slug",$brand)->first()->id;
                $brand_ids[] = $brand_id;
            }

        }


        if($request->get('category') != null){

            $categories = $request->get('category');
            $category_ids = [];

            foreach ($categories as $key => $category) {
                $category_id = Category::where("slug",$category)->first()->id;
                $category_ids[] = $category_id;
            }

            if($request->get('brand') != null){
                $vehicle_fits = VehicleFit::whereIn("category_id",$category_ids)->whereIn("brand_id",$brand_ids)->get();
            }else{
                $vehicle_fits = VehicleFit::whereIn("category_id",$category_ids)->get();
            }
            
            foreach ($vehicle_fits as $obj) {
                $vehicle_fit[] = $obj->product_id;
            }

        }else{
            if($request->get('brand') != null){
                $vehicle_fits = VehicleFit::whereIn("brand_id",$brand_ids)->get();
            }else{
                $vehicle_fits = VehicleFit::all();
            }
            foreach ($vehicle_fits as $obj) {
                $vehicle_fit[] = $obj->product_id;
            }
        }
        

        $products = Product::with('category')
        ->with('default_image')
        ->with('orders')
        ->with('singleorder')
        ->with('reviews')
        ->where("price",">=",$request->get('minprice'))
        ->where("price","<=",$request->get('maxprice'))
        ->where("title","LIKE","%".$request->get('search')."%")
        ->whereIn("id",$vehicle_fit)
        ->skip($skip)
        ->take($take)
        ->get();


        $products = $products->all();
        $products_count = Product::where("price",">",$request->get('minprice') - 1)->where("price","<",$request->get('maxprice') + 1)->where("title","LIKE","%".$request->get('search')."%")->whereIn("id",$vehicle_fit)->count();


        $minprice = Product::orderBy("price","asc")->first();
        $maxprice = Product::orderBy("price","desc")->first();


        return response()->json(compact('products','minprice','maxprice','products_count'));
    }

    public function getproduct($slug = null){


        $product = Product::with('category')->with('default_image')->with('orders')->with('singleorder')->with('reviews')->with('images')->where("slug",$slug)->first();

        $reviews = [];
        $totalreviews = ProductReview::where("product_id",$product->id)->get()->count();
        if($totalreviews != 0){
            $reviews["five"] = (ProductReview::where("product_id",$product->id)->where("review","5")->get()->count()/$totalreviews) * 100;
            $reviews["four"] = ((ProductReview::where("product_id",$product->id)->where("review","4")->get()->count()/$totalreviews)) * 100;
            $reviews["three"] = ((ProductReview::where("product_id",$product->id)->where("review","3")->get()->count()/$totalreviews)) * 100;
            $reviews["two"] = ((ProductReview::where("product_id",$product->id)->where("review","2")->get()->count()/$totalreviews)) * 100;
            $reviews["one"] = ((ProductReview::where("product_id",$product->id)->where("review","1")->get()->count()/$totalreviews)) * 100;
            $reviews["totalreviews"] = $totalreviews;
        }



        $get_product_recent_views = [];
        $get_user_rating = 0;
        $user = "";
        try {
            
            if ($user = JWTAuth::parseToken()->authenticate()) {
                $user = User::where("email",$user->email)->first();

                $_view = ProductRecentView::where("user_id",$user->id)->where("product_id",$product->id)->first();
                if($_view == null){
                    $_view = new ProductRecentView;
                    $_view->user_id = $user->id;
                    $_view->product_id = $product->id;
                    $_view->save();
                }else{
                    $_view->product_id = $product->id;
                    $_view->save();
                }

                $product_recent_views = ProductRecentView::where("user_id",$user->id)->orderBy("updated_at","desc")->take(5)->get();
            
                $product_recents = [];
                foreach ($product_recent_views as $obj) {
                    $product_recents[] = $obj->product_id;
                }

                $get_product_recent_views = Product::with('category')->with('default_image')->with('orders')->with('singleorder')->with('reviews')->with('images')->whereIn("id",$product_recents)->latest()->get();

                $get_user_rating = ProductReview::where("product_id",$product->id)->where("user_id",$user->id)->first();

                if($get_user_rating == null){
                    $get_user_rating = 0;
                }else{
                    $get_user_rating = $get_user_rating->review;
                }
            }
        
        } catch (\Throwable $th) {
                //throw $th;
        }

        

        return response()->json(compact('product','reviews','get_product_recent_views','get_user_rating','user'));
    }

    public function setrating(Request $request){
        $user_id = $request->get("user_id");
        $product_id = $request->get("product_id");
        $review = $request->get("review");

        $product_review = ProductReview::where("user_id",$user_id)->where("product_id",$product_id)->first();

        if($product_review == null){
            $product_review = new ProductReview;
        }
        
        $product_review->user_id = $user_id;
        $product_review->product_id = $product_id;
        $product_review->review = $review;

        $product_review->save();
    }

    public function getcart(){

        try {
            if ($user = JWTAuth::parseToken()->authenticate()) {
                $email = $user->email;
                $user = User::where("email",$user->email)->first();
                $normal = Order::with('product')->with('default_image')->where("user_id",$user->id)->where("cart","yes")->get(); 
                $custom = CustomOrder::where("user_id",$user->id)->where("cart","yes")->get();

                $cart_count = $normal->count() + $custom->count();


                $totalorder = Order::where("user_id",$user->id)->where("cart","yes")->where("total","!=",null)->where("total","!=","")->get(); 
                $total_customorder = CustomOrder::where("user_id",$user->id)->where("cart","yes")->where("total","!=",null)->where("total","!=","")->get(); 

                $totalorderprice = 0; $total_customorder_price = 0;
                foreach ($totalorder as $value) {
                    $totalorderprice = $totalorderprice + $value->total;
                }

                foreach ($total_customorder as $value) {
                    $total_customorder_price = $total_customorder_price + $value->total;
                }

                $totalprice = $totalorderprice + $total_customorder_price;

                return response()->json(compact('normal','custom','cart_count','totalprice','email'));
            }
        } catch (\Throwable $th) {

        }

    }

    public function paymentsuccessful(Request $request){

        try {
            if ($user = JWTAuth::parseToken()->authenticate()) {
                $email = $user->email;
                $user = User::where("email",$email)->first();


                $totalorder = Order::where("user_id",$user->id)->where("cart","yes")->where("total","!=",null)->where("total","!=","")->get(); 
                $total_customorder = CustomOrder::where("user_id",$user->id)->where("cart","yes")->where("total","!=",null)->where("total","!=","")->get(); 

                foreach ($totalorder as $value) {
                    $value->cart = "no";
                    $value->order_code = $request->get("reference");
                    $value->save();

                    $allorder = new AllOrder;
                    $allorder->user_id = $value->user_id;
                    $allorder->product_id = $value->product_id;
                    $allorder->product = $value->product_title;
                    $allorder->order_id = $value->id;
                    $allorder->order_type = "normal";
                    $allorder->order_code = $request->get("reference");
                    $allorder->tracking_status = "new";
                    $allorder->quantity = $value->quantity;
                    $allorder->price = $value->price;
                    $allorder->total = $value->total;
                    $allorder->slug = $value->slug;
                    $allorder->save();
                }
                foreach ($total_customorder as $value) {
                    $value->cart = "no";
                    $value->order_code = $request->get("reference");
                    $value->save();

                    $allorder = new AllOrder;
                    $allorder->user_id = $value->user_id;
                    $allorder->product_id = $value->id;
                    $allorder->product = $value->part_name;
                    $allorder->order_id = $value->id;
                    $allorder->order_type = "custom";
                    $allorder->order_code = $request->get("reference");
                    $allorder->tracking_status = "new";
                    $allorder->quantity = $value->quantity;
                    $allorder->price = $value->price;
                    $allorder->total = $value->total;
                    $allorder->vin = $value->vin;
                    $allorder->save();
                }


                $normal = Order::with('product')->with('default_image')->where("user_id",$user->id)->where("cart","yes")->get(); 
                $custom = CustomOrder::where("user_id",$user->id)->where("cart","yes")->get();

                $cart_count = $normal->count() + $custom->count();

                $official_email = Setting::where("skey","email")->first()->svalue;

                Mail::send("emails.paymentorder", [
                    'name' => $user->name,
                    'email' => $user->email,
                    'address' => $user->address,
                    'order_code' => $request->get("reference"),
                    'totalprice' => $request->get("totalprice"),
                    'totalorder' => $totalorder,
                    'total_customorder' => $total_customorder,
                    'date' => date('d/m/Y H:i:s')
                    ], function($message) use ($user,$official_email){
                                $message->subject('Order Successful - Autogig');
                                $message->bcc($official_email, 'Autogig');
                                $message->to($user->email,$user->name);
                                $message->replyTo($official_email);
                });



                return response()->json(compact('cart_count'));
            }
        } catch (\Throwable $th) {
            \Debugbar::error($th);
        }
    }

    public function getcustomproduct($id = null, $vin = null){

        try {
            if ($user = JWTAuth::parseToken()->authenticate()) {
                $user = User::where("email",$user->email)->first();
                $product = CustomOrder::where("id",$id)->where("vin",$vin)->where("cart","yes")->first();


                $custom_order_timer = Setting::where("skey","custom order timer")->first()->svalue;

                if($custom_order_timer == 0){
                    $custom_order_timer = 51;
                }


                $endtime = new \DateTime($product->created_at);
                $endtime->add(new \DateInterval('PT' . $custom_order_timer . 'M'));
                $endtime = $endtime->format('Y-m-d H:i:s');

                $currenttime = date("Y-m-d H:i:s");

                return response()->json(compact('product','endtime','currenttime'));
            }
        } catch (\Throwable $th) {
            
            \Debugbar::error($th);
        }

    }

    public function submitcustomorder(Request $request){

        try {
            if ($user = JWTAuth::parseToken()->authenticate()) {

                $user_name = $user->name;
                $user_email = $user->email;
                $user = User::where("email",$user->email)->first();


                
                $custom = new CustomOrder;

                $custom->user_id = $user->id;
                $custom->vin = $request->get("vin");
                $custom->brand = $request->get("brand");
                $custom->model = $request->get("model");
                $custom->year_selected = $request->get("year");
                $custom->trim = $request->get("trim");
                $custom->part_number = $request->get("part_no");
                $custom->part_name = $request->get("part_name");
                $custom->quantity = $request->get("quantity");
                $custom->comment = $request->get("comment");
                $custom->cart = "yes";

                $file =  $request->file("file");

                if($file != null){

            
                    $file_title = $file->getClientOriginalName();
                    
                    $file_ext = strtolower($file->getClientOriginalExtension());
                    $rand_code  = "0";
                    if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){
                
                        for($j=0; $j<9; $j++) {
                            $min = ($j == 0) ? 1:0;
                            $rand_code .= mt_rand($min,9);
                        }

                        $file->move('images/productimages/', $rand_code.'.'.$file_ext);
                
                        $custom->media = 'images/productimages/'.$rand_code.'.'.$file_ext;
                        
                    }

                }


                $custom->save();

                $id = CustomOrder::where("vin",$request->get("vin"))->where("user_id",$user->id)->latest()->first()->id;

                $official_email = Setting::where("skey","email")->first()->svalue;

                Mail::send("emails.customorder", [
                    'vin' => $request->get("vin"),
                    'brand' => $request->get("brand"),
                    'model' => $request->get("model"),
                    'year_selected' => $request->get("year"),
                    'trim' => $request->get("trim"),
                    'part_number' => $request->get("part_no"),
                    'part_name' => $request->get("part_name"),
                    'quantity' => $request->get("quantity"),
                    'comment' => $request->get("comment"),
                    'date' => date('d/m/Y H:i:s')
                    ], function($message) use ($official_email){
                                $message->subject('New custom order from http://autogig.com.ng');
                                $message->to($official_email);
                });


                Mail::send("emails.customorder_user", [
                    'vin' => $request->get("vin"),
                    'brand' => $request->get("brand"),
                    'model' => $request->get("model"),
                    'year_selected' => $request->get("year"),
                    'trim' => $request->get("trim"),
                    'part_number' => $request->get("part_no"),
                    'part_name' => $request->get("part_name"),
                    'quantity' => $request->get("quantity"),
                    'comment' => $request->get("comment"),
                    'user_name' => $user_name,
                    'date' => date('d/m/Y H:i:s')
                    ], function($message) use ($user_email){
                                $message->subject('New custom order from http://autogig.com.ng');
                                $message->to($user_email);
                });

                return response()->json(compact('id'));
            }
         } catch (\Throwable $th) {
         }
    }

    public function updatenormalcart(Request $request){
        $order = Order::where("id",$request->get('id'))->first(); 

        $order->quantity = $request->get('quantity');
        $order->price = $request->get('price');
        $order->total = $request->get('total');
        $order->cart = "yes";

        $order->save();
    }

    public function removenormalcart(Request $request){
        
        Order::where("id",$request->get('id'))->delete(); 

        try {
                if ($user = JWTAuth::parseToken()->authenticate()) {

                $user = User::where("email",$user->email)->first();
                $normal = Order::with('product')->with('default_image')->where("user_id",$user->id)->where("cart","yes")->get(); 
                $custom = CustomOrder::where("user_id",$user->id)->where("cart","yes")->get();

                $cart_count = $normal->count() + $custom->count();

                return response()->json(compact('normal','custom','cart_count'));
            }
        } catch (\Throwable $th) {

        }    
            
    }

    public function removecustomcart(Request $request){
        
        CustomOrder::where("id",$request->get('id'))->delete(); 

        try {
            if ($user = JWTAuth::parseToken()->authenticate()) {

                $user = User::where("email",$user->email)->first();
                $normal = Order::with('product')->with('default_image')->where("user_id",$user->id)->where("cart","yes")->get(); 
                $custom = CustomOrder::where("user_id",$user->id)->where("cart","yes")->get();

                $cart_count = $normal->count() + $custom->count();

                return response()->json(compact('normal','custom','cart_count'));
            }
        } catch (\Throwable $th) {

        }
    }

    public function addtonormalcart(Request $request){

        try {
                if ($user = JWTAuth::parseToken()->authenticate()) {

                    $user = User::where("email",$user->email)->first();

                    $order = Order::where("product_id",$request->get('id'))->where("cart","yes")->first(); 
                    if($order == null){

                        $order = new Order;

                        $order->product_id = $request->get('id');
                        $order->user_id = $user->id;
                        $order->quantity = 1;
                        $order->price = $request->get('price');
                        $order->total = $request->get('price');
                        $order->product_title = $request->get('title');
                        $order->slug = str_slug($request->get('title'), '-');
                        $order->cart = "yes";

                        $order->save();
                    }
                    
                    $normal = Order::with('product')->with('default_image')->where("user_id",$user->id)->where("cart","yes")->get(); 
                    $custom = CustomOrder::where("user_id",$user->id)->where("cart","yes")->get();

                    $cart_count = $normal->count() + $custom->count();

                    return response()->json(compact('normal','custom','cart_count'));
            }
        } catch (\Throwable $th) {
            \Debugbar::error($th);
        } 
    }


    public function getcategoryproducts($category_id = null){

        $products = Product::with('category')->with('default_image')->with('orders')->with('singleorder')->with('reviews')->where("category_id",$category_id)->get();

        $category = Category::where("id",$category_id)->first()->title;

        return response()->json(compact('products','category'));

    }

    public function deleteProduct($id){
        Product::where("id", $id)->delete();

        $success = "Product deleted successfully";

        return response()->json(compact('success'));
    }

    public function uploadProductImages(Request $request){

            $user = JWTAuth::parseToken()->authenticate();

            $media_id = $request->get("media_id");

            $file =  $request->file("fileupload");

            $getproduct = Product::where("media_id",$media_id)->first();

            if($getproduct != null){
                $getproduct_id = $getproduct->id;
            }else{
                $getproduct_id = null;
            }


            // $file_title = $file->getClientOriginalName();
            // $checktitle = ProductMedia::where("media_title",$file_title)->first();

            // if($checktitle){
            //     return response()->json("Image uploaded already",400);
            // }
    
            
            $file_ext = strtolower($file->getClientOriginalExtension());
            $rand_code  = "0";
            if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){
        
                for($j=0; $j<9; $j++) {
                    $min = ($j == 0) ? 1:0;
                    $rand_code .= mt_rand($min,9);
                }

                $file->move('images/productimages/', $rand_code.'.'.$file_ext);
        
                $productmedia = new ProductMedia;
                $productmedia->product_id =  $getproduct_id;
                $productmedia->user_id =  $user->id;
                $productmedia->media_title = $rand_code;
                $productmedia->media_id = $media_id;
                $productmedia->media = 'images/productimages/'.$rand_code.'.'.$file_ext;
                $productmedia->save();
                
            }else{
                $productmedia->image = "";
            }
          

        //return response()->json(compact('file'));
        return $rand_code;
             
    }

    public function uploadProductBrandLogos(Request $request){

        $user = JWTAuth::parseToken()->authenticate();

        $file =  $request->file("fileupload");


        $file_title = $file->getClientOriginalName();
        $checktitle = ProductBrandLogo::where("media_title",$file_title)->first();

        if($checktitle){
            return response()->json("Image uploaded already",400);
        }

        
        $file_ext = strtolower($file->getClientOriginalExtension());
        $rand_code  = "0";
        if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){
    
            for($j=0; $j<9; $j++) {
                $min = ($j == 0) ? 1:0;
                $rand_code .= mt_rand($min,9);
            }

            $file->move('images/productbrandimages/', $rand_code.'.'.$file_ext);
    
            $pbl = new ProductBrandLogo;
            $pbl->media_title = $file_title;
            $pbl->media = 'images/productbrandimages/'.$rand_code.'.'.$file_ext;
            $pbl->save();
            
        }else{
            $pbl->image = "";
        }
      

        return response()->json(compact('file'));
            
    }

    public function getproductbrandlogos(){
        $product_brand_logos = ProductBrandLogo::all();

        return response()->json(compact('product_brand_logos'));
    }

    public function gethomedata(){
        $product_brand_logos = ProductBrandLogo::all();

        $featuredproducts = Product::with('category')->with('default_image')->where("featured","yes")->get();

        $hotproducts = Product::with('category')->with('default_image')->where("hot","yes")->get();

        return response()->json(compact('product_brand_logos','featuredproducts','hotproducts'));
    }

    public function deleteProductBrandLogo(Request $request){
        $product_brand_logos = ProductBrandLogo::where("media_title",$request->get('title'))->get();

        foreach ($product_brand_logos as $product_brand_logo) {
            $path = base_path().'/'.$product_brand_logo->media; 
            unlink($path);
            $product_brand_logo->delete();
        }

        return "File removed";
    }

    public function getproductimages($id = null){
        $product_images = ProductMedia::where("product_id",$id)->get();

        return response()->json(compact('product_images'));
    }

    public function deleteProductMedia(Request $request){
        $products = ProductMedia::where("media_title",$request->get('title'))->get();

        foreach ($products as $product) {
            $path = base_path().'/'.$product->media; 
            unlink($path);
            $product->delete();
        }

        return "File removed";
    }

    public function addProduct(Request $request){

        $request->validate([
            'title' => 'required|string|max:255|unique:products',
            'price' => 'required',
            'category' => 'required',
            'brands_selected' => 'required',
            'featured' => new FeaturedproductCount,
            'hot' => new HotproductCount,
        ]);

        $product = new Product;
        $product->category_id = $request->get('category');
        $product->title = $request->get('title');
        $product->slug = str_slug($request->get('title'), '-');
        $product->specification = $request->get('specification');
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->media_id = $request->get('media_id');
        $product->featured = $request->get('featured');
        $product->hot = $request->get('hot');

        $product->save();
        
        $brands_selected = $request->get('brands_selected');
        $product_id = Product::where("title",$request->get('title'))->first()->id;
        foreach ($brands_selected as $brands) {
            $vehicle_fit = new VehicleFit;
            $vehicle_fit->product_id = $product_id;
            $vehicle_fit->brand_id = $brands["value"];
            $vehicle_fit->category_id = $request->get('category');
            $vehicle_fit->save();
        }

        $product_media = ProductMedia::where("media_id",$request->get('media_id'))->get();
        foreach ($product_media as $value) {
            $value->product_id = Product::where("media_id",$request->get('media_id'))->first()->id;
            $value->save();
        }

        $success = "Product created successfully";

        return response()->json(compact('success'));
    }

    public function updateProduct(Request $request){

        $product = Product::where("id",$request->get('id'))->first();

        if($product->title != $request->get('title')){

            $request->validate([
                'title' => 'required|string|max:255|unique:products',
                'price' => 'required',
            ]);

        }

        $request->validate([
            'featured' => new FeaturedproductCount,
            'hot' => new HotproductCount,
        ]);

        
        $product->category_id = $request->get('category');
        $product->title = $request->get('title');
        $product->description = $request->get('description');
        $product->price = $request->get('price');
        $product->media_id = $request->get('media_id');
        $product->featured = $request->get('featured');
        $product->hot = $request->get('hot');

        $product->save();

        $product_media = ProductMedia::where("media_id",$request->get('media_id'))->get();

        foreach ($product_media as $value) {
            $value->product_id = Product::where("media_id",$request->get('media_id'))->first()->id;
            $value->save();
        }




        $success = "Product created successfully";

        return response()->json(compact('success'));
    }

    public function editProduct($id){

        $product = Product::where("id",$id)->with('category')->first();

        $vehiclefit = VehicleFit::where("product_id",$id)->get();

        $brands = [];
        foreach ($vehiclefit as $fit) {
            $brand = Brand::where("id",$fit->brand_id)->first();
            $brands[] = $brand;
        }

        return response()->json(compact('product','brands'));
    }

    public function reviews(){

        $reviews = ProductReview::with('user')->with('product')->get();

        $all_count = ProductReview::get()->count();
        $five_star_count = ProductReview::where("review",5)->get()->count();
        $four_star_count = ProductReview::where("review",4)->get()->count();
        $three_star_count = ProductReview::where("review",3)->get()->count();
        $two_star_count = ProductReview::where("review",2)->get()->count();
        $one_star_count = ProductReview::where("review",1)->get()->count();

        return response()->json(compact(
            'reviews',
            'all_count',
            'five_star_count',
            'four_star_count',
            'three_star_count',
            'two_star_count',
            'one_star_count'
        ));

    }

    public function productreview($product_id = null, $review = null){

        if($review == ""){
            $reviews = ProductReview::with('user')->with('product')->where("product_id",$product_id)->get();
        }else{
            $reviews = ProductReview::with('user')->with('product')->where("review",$review)->where("product_id",$product_id)->get();
        }

        $product = Product::where("id",$product_id)->first();
        
        $all_count = ProductReview::where("product_id",$product_id)->get()->count();
        $five_star_count = ProductReview::where("review",5)->where("product_id",$product_id)->get()->count();
        $four_star_count = ProductReview::where("review",4)->where("product_id",$product_id)->get()->count();
        $three_star_count = ProductReview::where("review",3)->where("product_id",$product_id)->get()->count();
        $two_star_count = ProductReview::where("review",2)->where("product_id",$product_id)->get()->count();
        $one_star_count = ProductReview::where("review",1)->where("product_id",$product_id)->get()->count();

        return response()->json(compact(
            'reviews',
            'all_count',
            'five_star_count',
            'four_star_count',
            'three_star_count',
            'two_star_count',
            'one_star_count',
            'product'
        ));

    }

    public function userreview($user_id = null, $review = null){

        if($review == ""){
            $reviews = ProductReview::with('user')->with('product')->where("user_id",$user_id)->get();
        }else{
            $reviews = ProductReview::with('user')->with('product')->where("review",$review)->where("user_id",$user_id)->get();
        }

        $user = User::where("id",$user_id)->first();
        
        $all_count = ProductReview::where("user_id",$user_id)->get()->count();
        $five_star_count = ProductReview::where("review",5)->where("user_id",$user_id)->get()->count();
        $four_star_count = ProductReview::where("review",4)->where("user_id",$user_id)->get()->count();
        $three_star_count = ProductReview::where("review",3)->where("user_id",$user_id)->get()->count();
        $two_star_count = ProductReview::where("review",2)->where("user_id",$user_id)->get()->count();
        $one_star_count = ProductReview::where("review",1)->where("user_id",$user_id)->get()->count();

        return response()->json(compact(
            'reviews',
            'all_count',
            'five_star_count',
            'four_star_count',
            'three_star_count',
            'two_star_count',
            'one_star_count',
            'user'
        ));

    }

    public function review($review = null){

        $reviews = ProductReview::with('user')->with('product')->where("review",$review)->get();

        return response()->json(compact('reviews'));

    }

    public function update_user_role($user_id = null, $role = null){

        $user = User::where("id",$user_id)->first();

        if($user != null){
            $user->role = $role;
            $user->save();
        }

        $success = "Role updated successfully";

        return response()->json(compact('success'));

    }

    public function getcontentpages(){

        $contentpages = ContentPage::orderBy("title","asc")->get();

        return response()->json(compact('contentpages'));
    }

    public function getcontentpage($id = null){

        $contentpage = ContentPage::where("id", $id)->first();

        return response()->json(compact('contentpage'));
    }

    public function updatecontentpage(Request $request){

        $qleditor = QlEditor::where("page_id", $request->get("content_id"))->where("page_type","default")->get();

        $content = $request->get("content");

        $content = str_replace("&lt;","<", $content);
        $content = str_replace("&gt;",">", $content);

        foreach ($qleditor as $obj) {
            if (strpos($content, $obj->url) === false) {
                $url = str_replace(route('index'),base_path(),$obj->url);
                unlink($url);
                $obj->delete();
            }
        }


        $contentpage = ContentPage::where("id", $request->get("content_id"))->first();

        $contentpage->content = $content;

        $contentpage->save();

        $success = "Page updated successfully";

        return response()->json(compact('success'));
    }

    public function settings(){
        $facebook = Setting::where("skey","facebook")->first();
        $twitter = Setting::where("skey","twitter")->first();
        $instagram = Setting::where("skey","instagram")->first();
        $email = Setting::where("skey","email")->first();
        $custom_order_timer = Setting::where("skey","custom order timer")->first();

        return response()->json(compact('facebook','twitter','instagram','email','custom_order_timer'));
    }

    public function updatesettings(Request $request){
        $facebook = Setting::where("skey","Facebook")->first();
        $facebook->svalue = $request->get("facebook"); $facebook->save();


        $twitter = Setting::where("skey","Twitter")->first();
        $twitter->svalue = $request->get("twitter"); $twitter->save();

        
        $instagram = Setting::where("skey","instagram")->first();
        $instagram->svalue = $request->get("instagram"); $instagram->save();

        
        $email = Setting::where("skey","email")->first();
        $email->svalue = $request->get("email"); $email->save();

        
        $custom_order_timer = Setting::where("skey","custom order timer")->first();
        $custom_order_timer->svalue = $request->get("custom_order_timer"); $custom_order_timer->save();

    }

    public function sendmessage(Request $request){

        $request->validate([
            'name' => 'required|string|max:150',
            'email' => 'required|string|email|max:150',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $name = $request->get('name');
        $email = $request->get('email');
        $subject = $request->get('subject');
        $content = $request->get('message');


        $official_email = Setting::where("skey","email")->first()->svalue;

        Mail::send("emails.contactus", [
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'content' => $content,
            'date' => date('d/m/Y H:i:s')
            ], function($message) use ($name,$email,$official_email){
                        $message->subject('New email from Autogig contact us form');
                        $message->to($official_email);
                        $message->replyTo($email);
        });


        $success = "Message sent successfully";
        return response()->json(compact('success'));

    }

    public function footer(){
        $facebook = Setting::where("skey","Facebook")->first()->svalue;
        $twitter = Setting::where("skey","Twitter")->first()->svalue;
        $instagram = Setting::where("skey","instagram")->first()->svalue;
        $email = Setting::where("skey","email")->first()->svalue;

        $categories = Category::inRandomOrder()->take(4)->get();

        return response()->json(compact('categories','facebook','twitter','instagram','email'));
    }

    public function resetpassword(Request $request){

        $user = User::where("email",$request->get('email'))->first();

        if($user == null){
            $message = "error";
            return response()->json(compact('message'));
        }

        $rand_code = "";
        for($j=0; $j<30; $j++) {
            $min = ($j == 0) ? 1:0;
            $rand_code .= mt_rand($min,9);
        }

        $user->forgot_password = $rand_code;
        $user->save();

        $name = $user->name;
        $email = $user->email;
        $reset_password = route('index').'/reset-password'.'/'.$rand_code;

        Mail::send("emails.resetpassword", [
            'name' => $name,
            'email' => $email,
            'reset_password' => $reset_password,
            'date' => date('d/m/Y H:i:s')
            ], function($message) use ($name,$email){
                        $message->subject('Password reset from http://autogig.com.ng');
                        $message->to($email,$name);
        });


        $message = "success";
        return response()->json(compact('message'));

    }

    public function resetpassworddone(Request $request){

        $request->validate([
            'password' => 'required|min:3|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:3'
        ]);

        $user = User::where("forgot_password",$request->get("code"))->first();

        if($user == null){
            $message = "error";
            return response()->json(compact('message'));
        }

        $user->password = Hash::make($request->get('password'));
        $user->forgot_password = null;
        $user->save();

        $message = "success";
        return response()->json(compact('message'));

    }

    public function ql_editor_imageupload(Request $request){

        $page_id =  $request->get("page_id");

        $file =  $request->file("image");

        $file_title = $file->getClientOriginalName();
       
        $file_ext = strtolower($file->getClientOriginalExtension());
        $rand_code  = "0";
        if($file_ext == "jpeg" || $file_ext == "jpg" || $file_ext == "png"){
        
            for($j=0; $j<9; $j++) {
                $min = ($j == 0) ? 1:0;
                $rand_code .= mt_rand($min,9);
            }

            $file->move('images/ql_editor/', $rand_code.'.'.$file_ext);

            $url = route('index').'/'.'images/ql_editor/'.$rand_code.'.'.$file_ext;
        
                
        }else{
            $url = "";
        }

        $qleditor = new QlEditor;
        $qleditor->page_id = $page_id;
        $qleditor->page_type = "default";
        $qleditor->url = $url;

        $qleditor->save();


        return response()->json(compact('url'));
          

    }

}
